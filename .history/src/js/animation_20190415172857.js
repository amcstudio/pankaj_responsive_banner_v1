"use strict";
~(function() {
  var tl,
  ad = document.getElementById('mainContent'),
  bgExit = document.getElementById("bgExit");


    window.init = function() {
      playAnimation();
      bgExit.addEventListener("click", bgExitHandler);
    };

    window.addEventListener("orientationchange", function() {
      playAnimation();
    });


  function playAnimation() {
    tl = new TimelineMax({});
    tl.set(ad, { force3D: true })
      .to("#copyOne", 1, { opacity: 1, ease: Power2.easeInOut })
      .to("#copyOne", 1, { opacity: 0, ease: Power2.easeInOut }, "+=2")
 
      .to("#copyTwo", 0.8, { opacity: 1, ease: Power2.easeInOut })
    
      .to("#ctaContainer", 1, {opacity: 1,rotation: 0.01, ease: Power2.easeInOut})
  }

  function bgExitHandler(e) {
    e.preventDefault();
    window.open(window.clickTag)
  }


  function reload() {
    tlReset = new TimelineMax({});
    tlReset.set(['#mainContent *'],{ 'clearProps': 'all'})
            .set{['#copyOne','#copyTwo','#ctaContainer'], {opacity: 0} }
            .add(playAnimation)
  }
})();
