"use strict";
~(function() {
    var $ = TweenMax,
        tl,
        bgExit = document.getElementById("bgExit"),
        displacement = 250,
        ad = document.getElementById("mainContent");

    window.init = function() {
        bgExit.addEventListener("click", bgExitHandler);

        tl = new TimelineMax({});
        
       

    };

    function ctaArrowAnim() {
        var tll = new TimelineMax();
        tll.to("#arrow", 0.5, {
            x: 5,
            repeat: 15,
            yoyo: true,
            ease: Sine.easeInOut
        });
    }

    function bgExitHandler(e) {
        e.preventDefault();
        window.open(window.clickTag);
    }
})();