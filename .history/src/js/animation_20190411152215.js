"use strict";
~(function() {
    var $ = TweenMax,
        tl,
        bgExit = document.getElementById("bgExit"),
        displacement = 250,
        ad = document.getElementById("mainContent");

    window.init = function() {
        bgExit.addEventListener("click", bgExitHandler);

        tl = new TimelineMax({});
        
        tl.set("#copyTwo", { y: displacement });

        tl.to("#copyOne", 1, { opacity: 1, ease: Power2.easeInOut }, "+=2");
        tl.to("#animationContainer", 1, { y: -(displacement), rotation: 0.01, ease: Power2.easeInOut }, "+=2.7");
        tl.to("#copyOne", 1, { y: -(displacement), ease: Power2.easeInOut }, "-=1");
        tl.to("#copyTwo", 1, { y: 0, opacity: 1, ease: Power2.easeInOut }, "-=1");
        tl.to(["#copyOne", "#copyTwo"], 1, { opacity: 0, ease: Power2.easeInOut }, "+=2.8");
        tl.to("#animationContainer", 1, { x: -11, y: -100, scale: 0.57, rotationY:0.01, ease: Power2.easeInOut },"+=1");
        tl.to("#premiumBox", 1, { opacity: 1, ease: Power2.easeInOut },"+=0.5");

        tl.to("#lightBg", 1, { x: 0, ease: Sine.easeInOut }, "+=1");
        tl.to('#logo', 1, { opacity: 1, ease: Power2.easeInOut });

        tl.to(["#copyFour","#cta"], 1, { opacity: 1, ease: Power2.easeInOut, onComplete: ctaArrowAnim }, "-=1")

        tl.to("#copyFour", 1, { opacity: 0, ease: Power0.easeInOut }, "+=2.5");
        tl.to("#copyFive", 1, { opacity: 1, ease: Power0.easeInOut });
        tl.to("#ctaCopyOne", 1, { opacity: 0, ease: Power0.easeInOut },"-=2.2");

        tl.to("#ctaCopyTwo", 1, { opacity: 1, ease: Power2.easeInOut },"-=1.2");

    };

    function ctaArrowAnim() {
        var tll = new TimelineMax();
        tll.to("#arrow", 0.5, {
            x: 5,
            repeat: 15,
            yoyo: true,
            ease: Sine.easeInOut
        });
    }

    function bgExitHandler(e) {
        e.preventDefault();
        window.open(window.clickTag);
    }
})();