"use strict";
~(function() {
  var tl,
  ad = document.getElementById('mainContent'),
  bgExit = document.getElementById("bgExit"),
  tl = new TimelineMax({});


    window.init = function() {
      playAnimation();
      bgExit.addEventListener("click", bgExitHandler);
    };

    window.addEventListener("orientationchange", function() {
      reload();
      console.log("change")
    });


  function playAnimation() {
    tl//.set('#mainContent *',{ clearProps: 'all'})
      //.set(['#copyOne','#copyTwo','#ctaContainer'], {opacity: 0} )

      .set(ad, { force3D: true })
      .to("#copyOne", 1, { opacity: 1, ease: Power2.easeInOut })
      .to("#copyOne", 1, { opacity: 0, ease: Power2.easeInOut }, "+=2")
 
      .to("#copyTwo", 0.8, { opacity: 1, ease: Power2.easeInOut })
    
      .to("#ctaContainer", 1, {opacity: 1,rotation: 0.01, ease: Power2.easeInOut})
  }

  function bgExitHandler(e) {
    e.preventDefault();
    window.open(window.clickTag)
  }


  function reload() {
    
    var tlReset = new TimelineMax({});
    tlReset.pause(0, true)
            .remove()
    
      // .set('#mainContent *',{ clearProps: 'all'})
            // .set(['#copyOne','#copyTwo','#ctaContainer'], {opacity: 0} )
            .add(playAnimation)
  }
})();
